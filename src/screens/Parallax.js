import React from 'react';
import { StatusBar, View } from 'react-native';
import tw from 'twrnc';
import layer1 from '../assets/images/parallax/2.png';
import layer2 from '../assets/images/parallax/3.png';
import layer3 from '../assets/images/parallax/4.png';
import layer4 from '../assets/images/parallax/5.png';
import layer5 from '../assets/images/parallax/bg.jpeg';
import { ParallaxComponent, SensorAnimatedImage } from '../components';

const Parallax = () => {
    return (
        <View style={tw`flex-1 items-center justify-center`}>
            <ParallaxComponent layers={[layer1, layer2, layer3, layer4, layer5]} />
            <StatusBar style="dark" />
        </View>
    )
}

export default Parallax