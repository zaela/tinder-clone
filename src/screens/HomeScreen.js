import React, { useState } from 'react';
import { AnimatedStack } from '../components';
import TinderCard from '../components/molecules/TinderCard';
import users from '../dummy/users';

const HomeScreen = () => {
    const [back, setBack] = useState('f')
    const [right, setRight] = useState('f')
    const [left, setLeft] = useState('f')
    const [top, setTop] = useState('f')
    const onSwipeLeft = user => {
        // console.warn('swipe left', user.name);
        console.warn('swipe left');
    };
    
    const onSwipeRight = user => {
        // console.warn('swipe right: ', user.name);
        console.warn('swipe right: ');
    };

    const onSwipeBack = user => {
        console.warn('swipe back: ');
    };

    const onSwipeTop = user => {
        console.warn('swipe top: ');
    };

    return (
        <AnimatedStack
            data={users}
            renderItem={({item}) => <TinderCard user={item} />}
            onSwipeLeft={onSwipeLeft}
            onSwipeRight={onSwipeRight}
            onSwipeBack={onSwipeBack}
            onSwipeTop={onSwipeTop}
        />
    )
}

export default HomeScreen