import HomeScreen from './HomeScreen';
import MatchesScreen from './MatchesScreen';
import Parallax from './Parallax';

export {HomeScreen, MatchesScreen, Parallax};