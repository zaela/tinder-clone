import React from 'react';
import { Text, View } from 'react-native';
import tw from 'twrnc';
import { Messages, NewMatches } from '../components';
import Matches from '../dummy/newMatches';
import messagesHistory from '../dummy/messagesHistory';

const MatchesScreen = () => {
  console.log('NewMatches :', Matches)
  return (
    <>
      <View style={tw`w-full bg-white px-4`}>
         <View style={tw`w-full mt-4`}>
           <Text style={tw`text-sm font-semibold text-[#F76C6B]`}>NEW MATCHES</Text>
         </View>
      </View>
      <NewMatches users={Matches}/>
      <View style={tw`w-full bg-white px-4`}>
         <View style={tw`w-full mt-4`}>
           <Text style={tw`text-sm font-semibold text-[#F76C6B]`}>MESSAGES</Text>
         </View>
      </View>
      <Messages messages={messagesHistory}/>
    </>
  )
}

export default MatchesScreen