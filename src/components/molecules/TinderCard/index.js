import { View, Text, ImageBackground } from 'react-native';
import React from 'react';
import tw from 'twrnc';
// const 

const TinderCard = (props) => {
    const {name, image, bio} = props.user;
    return (
      <View style={[tw`w-full h-4/5 rounded-xl bg-[#fefefe] shadow-lg shadow-black`, {elevation:11}]}>
        <ImageBackground
          source={{
            uri: image,
          }}
          style={tw`w-full h-full`}
          imageStyle={tw`flex rounded-xl justify-end overflow-hidden`}
        >
          <View style={tw`p-4 absolute bottom-3`}>
            <Text style={tw`text-white text-xl font-semibold`}>{name}</Text>
            <Text style={tw`text-white text-lg`}>{bio}</Text>
          </View>
        </ImageBackground>
      </View>
    );
}

export default TinderCard