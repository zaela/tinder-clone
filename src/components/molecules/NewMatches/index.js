import { View, Text, Image, FlatList } from 'react-native'
import React from 'react'
import { FlashList } from '@shopify/flash-list'
import tw from 'twrnc'

const NewMatches = ({users}) => {
    const NewMatch = ({user}) => {
        return (
            <View style={tw`mr-4`}>
                <Image style={tw`w-16 h-16 rounded-full ${user.isNew === true ? 'border-4 border-[#F76C6B]' : ''}`} source={{uri:user?.image}} />
            </View>
        )
    }
    return (
        <View style={tw`bg-white px-4 py-3`}>
            <FlashList
              data={users}
              renderItem={({ item }) => <NewMatch user={item}  />}
              keyExtractor={(item) => item.id+'user'}
              horizontal
              showsHorizontalScrollIndicator={false}
              estimatedItemSize={10}
              rowWrapperStyle={tw`flex-row justify-around`}
            />
        </View>
    )
}

export default NewMatches