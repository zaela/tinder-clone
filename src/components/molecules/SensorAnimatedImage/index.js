import React from 'react';
import { useWindowDimensions } from 'react-native';
import Animated, { interpolate, SensorType, useAnimatedSensor, useAnimatedStyle, withTiming } from 'react-native-reanimated';

const IMAGE_OFFSET = 100;
const PI = Math.PI;
const HALF_PI = PI / 2

const SensorAnimatedImage = ({ image, order }) => {
    const {width, height} = useWindowDimensions()
    // console.log(width, height)

    const sensor = useAnimatedSensor(SensorType.ROTATION, {interval:1000})
    const imageStayle = useAnimatedStyle(() => {
        // console.log(sensor.sensor.value)
        const {yaw, pitch, roll} = sensor.sensor.value
        console.log(yaw.toFixed(1), pitch.toFixed(1), roll.toFixed(1))
        return {
            // top:pitch * 50,
            top: withTiming(
                interpolate(
                    pitch, 
                    [-HALF_PI, HALF_PI], 
                    [(-IMAGE_OFFSET * 2) / order, 0]
                ), 
                {duration:100}
            ),
            // left:roll * 50,
            left: withTiming(
                interpolate(roll, [-PI, PI], [(-IMAGE_OFFSET*2) / order, 0]), 
                {
                    duration:100,
                }
            ),
        };
    })

    return (
            <Animated.Image source={image} style={[{
                width:width + (2*IMAGE_OFFSET) / order, 
                height:height + (2*IMAGE_OFFSET) / order,
                position:'absolute',
            }, imageStayle]}/>
    )
}

export default SensorAnimatedImage