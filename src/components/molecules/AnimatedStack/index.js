import React, {useState, useEffect} from 'react';
import {View, StyleSheet, useWindowDimensions, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import Animated, {
  useSharedValue,
  useAnimatedStyle,
  useDerivedValue,
  useAnimatedGestureHandler,
  interpolate,
  withSpring,
  runOnJS,
} from 'react-native-reanimated';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Like from '../../../assets/images/LIKE.png';
import Nope from '../../../assets/images/nope.png';
import tw from 'twrnc'

const ROTATION = 60;
const SWIPE_VELOCITY_X = 800;
const SWIPE_VELOCITY_Y = 1500;

const AnimatedStack = ({data, renderItem, onSwipeRight, onSwipeLeft, onSwipeBack, onSwipeTop}) => {
  // const {data, renderItem, onSwipeRight, onSwipeLeft, back, setBack, right, setRight, left, setLeft, top, setTop} = props;

  const [currentIndex, setCurrentIndex] = useState(0);
  const [nextIndex, setNextIndex] = useState(1);
  
  const currentProfile = data[currentIndex];
  const nextProfile = data[nextIndex];

  const {width: screenWidth, height:screenHeight} = useWindowDimensions();

  const hiddenTranslateX = 2 * screenWidth;
  const hiddenTranslateY = 2 * screenHeight;

  const translateX = useSharedValue(0);
  const translateY = useSharedValue(0);
  // console.log('screenHeight :', screenHeight)
  // console.log('screenWidth :', screenWidth)
  // console.log('translateY :', translateY.value)

  const rotate = useDerivedValue(
    () =>
      interpolate(translateX.value, [0, hiddenTranslateX], [0, ROTATION]) +
      'deg',
  );

  const cardStyle = useAnimatedStyle(() => ({
    transform: [
      {
        translateX: translateX.value,
      },
      {
        translateY: translateY.value,
      },
      {
        rotate: rotate.value,
      },
    ],
  }));

  const nextCardStyle = useAnimatedStyle(() => {
    console.log('translateY.value :', translateY.value)
    if (-translateY.value > 0) {
      return {
        transform: [
          {
            scale: interpolate(
              translateY.value,
              [-hiddenTranslateY, 0, hiddenTranslateY],
              [1, 0.8, 1],
            ),
          },
        ],
        opacity: interpolate(
          translateY.value,
          [-hiddenTranslateY, 0, hiddenTranslateY],
          [1, 0.5, 1],
        ),
      }
    } else {
      return {
        transform: [
          {
            scale: interpolate(
              translateX.value,
              [-hiddenTranslateX, 0, hiddenTranslateX],
              [1, 0.8, 1],
            ),
          },
        ],
        opacity: interpolate(
          translateX.value,
          [-hiddenTranslateX, 0, hiddenTranslateX],
          [1, 0.5, 1],
        ),
      }
    }
  });

  const likeStyle = useAnimatedStyle(() => ({
    opacity: interpolate(translateX.value, [0, hiddenTranslateX / 5], [0, 1]),
  }));

  const superLikeStyle = useAnimatedStyle(() => ({
    opacity: interpolate(translateY.value, [0, hiddenTranslateY / 5], [0, 1]),
  }));

  const nopeStyle = useAnimatedStyle(() => ({
    opacity: interpolate(translateX.value, [0, -hiddenTranslateX / 5], [0, 1]),
  }));

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, context) => {
      context.startX = translateX.value;
      context.startY = translateY.value;
    },
    onActive: (event, context) => {
      translateX.value = context.startX + event.translationX;
      if (event.translationY < 0) {
        translateY.value = context.startY + event.translationY;
      }
      // console.log('event.velocityX :', event.velocityX)
      // console.log('event.translationY :', event.translationY)
    },
    onEnd: event => {
      // console.log('event.velocityY :', -event.velocityY)
      if (-event.velocityY > 90) {
        if(-event.velocityY < SWIPE_VELOCITY_Y){
          translateX.value = withSpring(0);
          translateY.value = withSpring(0);
          return;
        }
      }else{
        if (Math.abs(event.velocityX) < SWIPE_VELOCITY_X) {
          // console.log('event.velocityX :', -event.velocityX)
          translateX.value = withSpring(0);
          translateY.value = withSpring(0);
          return;
        }
      }
      

      translateX.value = withSpring(
        hiddenTranslateX * Math.sign(event.velocityX),
        {},
        () => {
          runOnJS(setCurrentIndex)(currentIndex + 1)
          runOnJS(setNextIndex)(currentIndex + 2)
          translateX.value = 0;
        },
      );

      translateY.value = withSpring(
        hiddenTranslateX * Math.sign(event.velocityY),
        {},
        () => {
          runOnJS(setCurrentIndex)(currentIndex + 1)
          runOnJS(setNextIndex)(currentIndex + 2)
          translateY.value = 0;
        },
      );
      
      const onSwipe = -event.velocityY > 0 ? onSwipeTop : (event.velocityX > 0 ? onSwipeRight : onSwipeLeft);
      onSwipe && runOnJS(onSwipe)(currentProfile);
    },
  });

  // useEffect(() => {
  //   translateX.value = 0;
  //   setNextIndex(currentIndex + 1);
  // }, [currentIndex, translateX]);

  // console.log('currentIndex :', currentIndex)
  // console.log('nextIndex :', nextIndex)

  const back = () => {
    translateX.value = 0;
    if (currentIndex && nextIndex > 0) {
      setCurrentIndex(currentIndex - 1);
      onSwipeBack()
    }
  }
  
  const right = () => {
    translateX.value = withSpring(
      800, 
      {},
      () => { 
        runOnJS(setCurrentIndex)(currentIndex + 1)
        runOnJS(setNextIndex)(currentIndex + 2)
        translateX.value = 0;
      },
    );
    onSwipeRight()
  }
  const left = () => {
    translateX.value = withSpring(
      -800, 
      {},
      () => { 
        runOnJS(setCurrentIndex)(currentIndex + 1)
        runOnJS(setNextIndex)(currentIndex + 2)
        translateX.value = 0;
      },
    );
    onSwipeLeft()
  }

  const top = () => {
    // translateX.value = 0;
    //   if (currentIndex < data.length && nextIndex < data.length) {
    //     setCurrentIndex(currentIndex + 1);
    //     onSwipeTop()
    //   }
    translateY.value = withSpring(
      -1500, 
      {},
      () => { 
        runOnJS(setCurrentIndex)(currentIndex + 1)
        runOnJS(setNextIndex)(currentIndex + 2)
        translateY.value = 0;
      },
    );
    onSwipeTop()
  }

  return (
    <>
      <View style={tw`w-full flex-1 bg-[##eeeeee] justify-center items-center px-4`}>
        <View style={tw`flex justify-center items-center w-full`}>
          {nextProfile && (
            <View style={[tw`justify-center items-center`, {...StyleSheet.absoluteFillObject}]}>
              <Animated.View style={[tw`w-full h-full justify-center items-center`, nextCardStyle]}>
                {renderItem({item: nextProfile})}
              </Animated.View>
            </View>
          )}

          {currentProfile && (
            <PanGestureHandler onGestureEvent={gestureHandler}>
              <Animated.View style={[tw`w-full h-full justify-center items-center`, cardStyle]}>
                <Animated.Image
                  source={Like}
                  style={[styles.like, {left: 10}, likeStyle]}
                  resizeMode="contain"
                />
                <Animated.Image
                  source={Nope}
                  style={[styles.like, {right: 10}, nopeStyle]}
                  resizeMode="contain"
                />
                {renderItem({item: currentProfile})}
              </Animated.View>
            </PanGestureHandler>
          )}
        </View>
      </View>
      <View style={tw`flex-row bg-white justify-around pb-4 pt-4`}>
        <TouchableOpacity onPress={() => back()} style={tw`bg-white p-2 rounded-full shadow-lg shadow-black`}>
            <FontAwesome name="undo" size={24} color="#FBD888" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => left()} style={tw`bg-white p-2 rounded-full shadow-lg shadow-black`}>
            <Entypo name="cross" size={24} color="#F76C6B" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => top()} style={tw`bg-white p-2 rounded-full shadow-lg shadow-black`}>
            <FontAwesome name="star" size={24} color="#3AB4CC" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => right()} style={tw`bg-white p-2 rounded-full shadow-lg shadow-black`}>
            <FontAwesome name="heart" size={24} color="#4FCC94" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}} style={tw`bg-white p-2 rounded-full shadow-lg shadow-black`}>
            <Ionicons name="flash" size={24} color="#A65CD2" />
        </TouchableOpacity>
      </View>
    </>

  );
};

const styles = StyleSheet.create({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width: '100%',
  },
  animatedCard: {
    width: '90%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nextCardContainer: {
    ...StyleSheet.absoluteFillObject,

    justifyContent: 'center',
    alignItems: 'center',
  },
  like: {
    width: 150,
    height: 150,
    position: 'absolute',
    top: 10,
    zIndex: 1,
    // elevation: 1,
  },
});

export default AnimatedStack;
