import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import tw from 'twrnc';

const Icon = ({label, focus}) => {
  switch (label) {
    case 'Home':
      return focus ? <Fontisto name="tinder" size={24} color="#F76C6B" /> : <Fontisto name="tinder" size={24} color="#bbbbbb" />;
    case 'Liked':
      return focus ? <MaterialCommunityIcons name="star-four-points" size={24} color="#F76C6B" /> : <MaterialCommunityIcons name="star-four-points" size={24} color="#bbbbbb" />;
    case 'Matches':
      return focus ? <Ionicons name="ios-chatbubbles" size={24} color="#F76C6B" /> : <Ionicons name="ios-chatbubbles" size={24} color="#bbbbbb" />;
    case 'Account':
      return focus ? <FontAwesome name="user" size={24} color="#F76C6B" /> : <FontAwesome name="user" size={24} color="#bbbbbb" />;
    default:
      return <Fontisto name="tinder" size={24} color="#F76C6B" />;
  }
};

const TopNavigator = ({state, descriptors, navigation}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={tw`flex flex-row justify-around pt-4 pb-4 bg-white`}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={tw`flex items-center`}  
          >
            <Icon label={label} focus={isFocused} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default TopNavigator;
