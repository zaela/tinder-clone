import { View, Text, Image, FlatList, ScrollView, useWindowDimensions } from 'react-native'
import React from 'react'
import { FlashList } from '@shopify/flash-list'
import tw from 'twrnc'
// const {width, height} = useWindowDimensions()

const Messages = ({messages}) => {
    const NewMatch = ({message}) => {
        return (
            <View style={tw`mr-4 w-full flex-row gap-4 py-2`}>
                <Image style={tw`w-16 h-16 rounded-full`} source={{uri:message?.image}} />
                <View style={tw`flex justify-center`}>
                    <Text style={tw`text-base text-gray-800`}>{message?.name ?? '-'}</Text>
                    <Text style={tw`text-sm text-gray-400`}>{message?.message ?? '-'}</Text>
                </View>
            </View>
        )
    }
    return (
        <FlashList
            data={messages}
            renderItem={({ item }) => <NewMatch message={item}  />}
            keyExtractor={(item) => item.id+'user'}
            contentContainerStyle={tw`bg-white px-4 py-3`}
            showsVerticalScrollIndicator={false}
            //   columnWrapperStyle={tw``}
            estimatedItemSize={10}
        />
    )
}

export default Messages