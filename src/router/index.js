import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { HomeScreen, MatchesScreen, Parallax } from '../screens';
import { TopNavigator } from '../components';

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={(props) => <TopNavigator {...props} />}>
      <Tab.Screen name="Home" options={{ headerShown: false }} component={HomeScreen} />
      <Tab.Screen name="Liked" options={{ headerShown: false }} component={HomeScreen} />
      <Tab.Screen name="Matches" options={{ headerShown: false }} component={MatchesScreen} />
      <Tab.Screen name="Account" options={{ headerShown: false }} component={HomeScreen} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator>
        <Stack.Screen
            name="MainApp"
            component={MainApp}
            options={{headerShown: false}}
        />
        {/* <Stack.Screen
            name="Parallax"
            component={Parallax}
            options={{headerShown: false}}
        /> */}
    </Stack.Navigator>
  )
}

export default Router