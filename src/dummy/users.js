export default [
    {
        name:'Ariel Tatum',
        image: 'https://cdn1-production-images-kly.akamaized.net/rvgP0NBweHomIPICFONCe4ZKM8g=/2x43:700x974/469x625/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/3793914/original/068137800_1640265770-11.jpg',
        bio:'Actrees, Model and Singer'
    },
    {
        name:'Pevita Pierce',
        image: 'https://cdns.klimg.com/kapanlagi.com/p/3200967658510866328753611916939240350835975n.jpg',
        bio:'Actrees & Model'
    },
    {
        name:'Dian Sastro',
        image: 'https://cdn1-production-images-kly.akamaized.net/Mdg38S7cVNRLr9pAwFB1PmYSk9E=/640x853/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3344361/original/053123600_1610162702-Dian_Sastrowardoyo_7.jpg',
        bio:'Actrees & Model'
    },
    {
        name:'Vonny Vonzy',
        image: 'https://img.sportstars.id/2022/08/e25S2e/master_B9p200dx7X_733_vonny_felicia_ba_dari_onic_esports_sumber_instagram.jpg',
        bio:'Gamer & Influencer'
    },
]

