import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { View, Text } from 'react-native';
import 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import tw from 'twrnc';
import Router from './router';

const MainApp = () => {
    return (
        <NavigationContainer>
            <Router />
        </NavigationContainer>
    );
};
  

const App = () => {
  return (
    <SafeAreaView style={tw`flex-1 bg-[#eeeeee]`}>
      {/* <View style={tw`flex-row bg-white justify-around pt-4 pb-4 shadow-lg shadow-black`}>
          <Fontisto name="tinder" size={24} color="#FBD888" />
          <MaterialCommunityIcons name="star-four-points" size={24} color="#F76C6B" />
          <Ionicons name="ios-chatbubbles" size={24} color="#3AB4CC" />
          <FontAwesome name="user" size={24} color="#4FCC94" />
      </View> */}
      <MainApp />
    </SafeAreaView>
  );
};

export default App;